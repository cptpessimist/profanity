FROM archlinux:base
RUN pacman -Sy --noconfirm &&\ 
    pacman -S pacman --noconfirm &&\
    pacman-db-upgrade &&\
    pacman -S --noconfirm profanity
ENTRYPOINT ["profanity"]
